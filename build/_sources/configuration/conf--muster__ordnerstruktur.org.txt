:PROPERTIES:
:ROAM_ALIASES: "Konfiguration  -- Download (Museter)"
:ID:       1b5723cf-bb53-418c-8d2f-551c602d602b
:END:
#+title:      Muster --  Ordner-Struktur
#+date:       [2024-04-30 Di 21:24]
#+identifier: 20240430T212412
#+STARTUP: overview
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+BEGIN_export rst
.. _20240430T212412:
.. INDEX:: Konfiguration; Download (Muster)
.. INDEX:: Download (Muster); Konfiguration
#+END_export

*Die verwendete Ordner-Struktur*

Auf einem Linux bzw. Mac-Rechner ist der HOME-Ordner ein Standard, für
Windows adaptiere ich teilweise das Unix-System, indem ich hier auch
eine HOME-Variable definiere. Nachfolgend die vorhandenen oder
vorgeschlagenen Einstellungen. Generell wird der Einstiegsordner über
die HOME-Variable angesprochen.

#+BEGIN_export rst

.. tab:: Linux

   Standard ist der HOME-Ordner aller Linux-Versionen:

   .. code:: bash

      /home/<benutzer>/aileen
      
.. tab:: MacOS

   Standard ist der User-Ordner :

   .. code:: bash

      /Users/<benutzer>/aileen

.. tab:: Windows

   Für Windwos immitiere ich gern ein  wenig das Unix-System, das
   macht einen späteren Umstieg einfacher und ein kurzer Pfad
   erleichtert den Umgang mit Pfadanaben.
   
   Linuxlike lege ich mir hierfür einen home-Ordner, direkt im Root
   der ersten Festplatte (meistens Laufwerk C:\) an.
   Weil es ein Einzelnutzerbetriebssystem ist, verzichte ich  hier auf
   den Benutzer-Ordner im HOME, der unter Linux für jeden Benutzer
   verfügbar ist:
	     
   .. code:: bash
      
      C:\home\aileen

   Prüfen ob die Umgebungsvariable HOME gesetzt ist:

   .. code:: bash

      C:>echo %HOME%
      c:\home

   Wenn HOME keinen Wert hat, über die Systemeinstellungen, Umgebungsvariablen setzen.

   
#+END_export
#+begin_org
[[id:50de95f5-cbb0-4f6b-89be-2c03bb18eb04][Konfiguration -- allgemein]]
#+end_org
