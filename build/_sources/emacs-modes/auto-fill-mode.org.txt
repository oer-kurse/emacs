:PROPERTIES:
:ROAM_ALIASES: "Auto-Fill -- Mode"
:ID:           130a05e5-5243-443e-8429-51735baa6a3d
:END:
#+title:       Auto-Fill-Mode
#+date:        [2024-11-08 Fr 11:33]
#+identifier:  20241108T113338
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil

#+BEGIN_export rst
.. _130a05e5-5243-443e-8429-51735baa6a3d:
.. INDEX:: Auto-Fill; Mode
.. INDEX:: Mode; Auto-Fill
.. raw:: html

   <style>

   .orange {
      color: orange;
   }
   
   </style>

.. role:: orange

.. meta::

   :description lang=de: Es gibt für dievers Aufgaben im Emacs spezielle Modes z.B. auto-fill-mode 
   :keywords: auto-fill-mode, EMACS, emacs, 


#+END_export

Der »auto-fill-mode« sorgt dafür, daß der Text auf die als Zeilenlänge
eingestellte Breite automatisch umgebrochen wird. Man kann also munter
drauf los schreiben und wird die Zeile zu lang, geht es auf der nächsten
Zeile weiter.

Dieses Verhalten wird in vielen Org-Konfigurationsvorschlägen auch
schon gezeigt:

#+begin_src bash
  (org-mode . auto-fill-mode)
#+end_src


#+begin_export rst
Manuell ein- bzw. ausschalten mit: :kbd:`ESC-x`, :orange:`auto-fill-mode` :kbd:`⏎`
#+end_export
