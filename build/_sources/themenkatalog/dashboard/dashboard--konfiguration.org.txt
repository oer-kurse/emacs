:PROPERTIES:
:ROAM_ALIASES: "Dashboard -- mit  -- Überblick"
:ID:            2a24b749-3060-4153-9304-e1f272ffa122
:END:
#+title:       Dashboard -- Konfiguration
#+date:        [2025-01-22 Mi 18:40]
#+filetags:    :dashboard:
#+identifier:  20250122T184048
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _2a24b749-3060-4153-9304-e1f272ffa122:
.. INDEX:: Dashboard; Überblick
.. INDEX:: Überblick; Dashboard
#+END_export

Die Konfiguration mit einer Zusatzfunktion für die Sprücheauswahl im
Footer (Zeilen 2-42). Danach einige Einstellungen die ich von der
Projektseite übernommen habe (Zeilen 44-60).

#+begin_export rst

.. code-block::
   :linenos:


    #+begin_src emacs-lisp
      (defun load-footer-messages-from-file (file-path)
        "Lädt Nachrichten aus einer Datei, die durch eine Zeile mit `%` getrennt sind,
         und gibt sie als Liste von Nachrichten zurück, die auch Zeilenumbrüche
         enthalten dürfen."
	 
        (with-temp-buffer
          ;; Dateiinhalt einlesen
          (insert-file-contents file-path)
          (goto-char (point-min))
    
          ;; Liste für die Nachrichten initialisieren
          (setq current-message "")
          (setq message-list '())
    
          ;; Zeile für Zeile lesen
          (while (not (eobp))
            (let ((line (thing-at-point 'line t)))
	      ;; Zeilenumbrüche und führende/folgende Leerzeichen entfernen
              (setq line (string-trim line))
              ;; Wenn die Zeile nur ein '%' ist, fügen wir die Nachricht zur Liste hinzu
              (if (string= line "%")
                  (progn
                    ;; Wenn current-message nicht leer ist, fügen wir sie zur Liste hinzu
                    (unless (string-empty-p current-message)
                      (push current-message message-list))
                    ;; Aktuelle Nachricht zurücksetzen
                    (setq current-message ""))
                ;; Ansonsten fügen wir die Zeile zur aktuellen Nachricht hinzu
                ;; Wenn es nicht die erste Zeile ist, fügen wir einen Zeilenumbruch hinzu
                (setq current-message (concat current-message
                                              (if (string-empty-p current-message) "" "\n")
                                              line))))
            (forward-line 1))
    
          ;; Falls noch eine Nachricht übrig ist, diese hinzufügen
          (unless (string-empty-p current-message)
            (push current-message message-list))
    
          ;; Umkehren der Liste, weil wir mit 'push' von hinten nach vorne hinzugefügt haben
          (reverse message-list)))
    
       
          (use-package dashboard
            :ensure t
            :config
            (dashboard-setup-startup-hook)
            (setq dashboard-items '((recents   . 2)
    
                			  (bookmarks . 5)
                			  (projects  . 5)
                			  (agenda    . 15)))
            (setq dashboard-item-names '(("Recent Files:"               . "Zuletzte geöffnet:")
                                         ("Agenda for the coming week:" . "Wichtige Termine:")
                                         ("Bookmarks:"                  . "Lesezeichen:")
                			     ("Projects:"                   . "Projekte:")))
            (setq dashboard-startup-banner "~/aileen/mein-bild.svg")
            (setq dashboard-filter-agenda-entry 'dashboard-no-filter-agenda)
            (setq dashboard-footer-messages (load-footer-messages-from-file "~/aileen/sprueche.txt"))
      	)    
    #+end_src
    #+end_export
