:PROPERTIES:
:ROAM_ALIASES: "Pakete installieren/verwalten"
:ID:           0060392b-4dc3-4624-9545-8005c67bdef0
:END:
#+title:      Pakete -- verwalten
#+date:       [2024-05-01 Mi 07:36]
#+identifier: 20240501T073600
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+BEGIN_export rst
.. _20240501T073600:
.. INDEX:: Pakete; Verwaltung
.. INDEX:: Verwaltung; Pakete


.. meta::

   :description lang=de: EMACS Paketverwaltung
   :keywords: EMACS, emacs, pakete installieren


.. toctree::
   :maxdepth: 1
   :glob:

   *

   
#+END_export
