:PROPERTIES:
:ROAM_ALIASES: "Pomdoro -- org-mode -- Timer"
:ID:           5cee4db6-4d4d-452c-9147-c6e8c8bf799f
:END:
#+title:       Pomdoro -- Zeitmangement
#+date:        [2025-01-18 Sa 18:34]
#+filetags:    ::
#+identifier:  20250118T183412
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _5cee4db6-4d4d-452c-9147-c6e8c8bf799f:
.. INDEX:: Pomdoro; Timer
.. INDEX:: Timer; Pomdoro


.. meta::

   :description lang=de: Pomodoro im Emacs, Zeitmangement visuell und akustisch.
   :keywords: Emacs, Pomodoro, visuelles Feedback, akustisches Feedback 

#+END_export

Die Pomodoro-Technik (orig. pomodoro technique von italienisch
pomodoro = Tomate und englisch technique = Methode, Technik) ist eine
Methode des Zeitmanagements, die von Francesco Cirillo in den 1980er
Jahren entwickelt wurde. Das System verwendet einen Kurzzeitwecker, um
Arbeit in 25-Minuten-Abschnitte – die sogenannten pomodori – und
Pausenzeiten zu unterteilen. Der Name pomodoro stammt von der
Küchenuhr, die Cirillo bei seinen ersten Versuchen benutzte.

Die Methode basiert auf der Idee, dass häufige Pausen die geistige
Beweglichkeit verbessern können [fn:1].

Siehe auch: https://youtu.be/JbHE819kVGQ

** Erweiterung

Visuelles Feedback erzeugen, wenn das akustische Signal nicht zur
Verfügung steht:

- einmal kurz in den light-Modus umschaltet und anschließend wieder in
  den dark-Mode zurück.
- oder den light-Modus aktivieren, als Meldung dann der Hinweis, wie
  der dark-Mode wieder aktiviert werden kann.
  Zeigt das Ende auch an, wenn man nicht am Gerät ist.
   
- Timer setzen: M-x org-timer-set-timer  HH:MM:SS
- alternativ die Tastenkombination: C-c  C-x ;

#+begin_src emacs-lisp

  ;; zufällige Auswahl aus einer Sammlung von wav-Dateien 
  (setq org-clock-sound
  	  (nth (random (length (directory-files "~/aileen/sound" t "\\.wav$"))) 
                           (directory-files "~/aileen/sound" t "\\.wav$")))

  ;; eine konkrete Sound-Datei 
  ;; (setq org-clock-sound "/home/ehp/aileen/sound/uhu.wav")

    (defun switch-to-light-mode-temporarily ()
      "Wechselt kurz in den Light-Mode und kehrt dann zum Dark-Mode zurück."
      (interactive)

      ;; Light-Mode aktivieren
      (load-theme 'standard-light t)
      
      ;; anschließend eine Meldung im Mini-Buffer einblenden
      (message 'Esc, x standard-themes-load-dark. Aktueller Sound: %s'
  	   (file-name-nondirectory org-clock-sound))
    )

    ;; einfache Message
    ;; (message 'Esc, x standard-themes-load-dark))

    ;; alternativ nach einer Sekunde wieder in den dark-Mode zurück
    ;; (run-with-timer 1 nil (lambda () (load-theme 'standard-dark t))))

    ;; Timer setzen
    (add-hook 'org-timer-done-hook 'switch-to-light-mode-temporarily)

#+end_src

#+RESULTS:
| switch-to-light-mode-temporarily |

* Footnotes

[fn:1] https://de.wikipedia.org/wiki/Pomodoro-Technik
