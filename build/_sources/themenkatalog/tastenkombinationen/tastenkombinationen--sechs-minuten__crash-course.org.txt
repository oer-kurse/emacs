:PROPERTIES:
:ROAM_ALIASES: "Tastenkombinationen -- Crash Course"
:ID:           43914832-835a-4f86-9df8-1737cf7ea7a9:
:END:
#+title:       Tastenkombinationen -- Crash Course -- Video
#+date:        2024-06-18 Di 05:11]
#+identifier:  20240618T051113
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _43914832-835a-4f86-9df8-1737cf7ea7a9:
.. INDEX:: Tastenkombinationen; Crash Course
.. INDEX:: Crash Course; Tastenkombinationen
#+END_export

Die wichtigsten Tastenkombinationen in sechs Minuten.

#+begin_export rst
.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/lAgj9Ean1fo?si=dMejMeWjtZzlwjJq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
   </iframe>

#+end_export
