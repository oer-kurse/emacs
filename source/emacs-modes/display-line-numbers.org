:PROPERTIES:
:ROAM_ALIASES: "line-number-mode"
:ID:           88c37c32-6ddc-4ed7-9737-3a3c1b78a1d3
:END:
#+title:       Display-Line-Number-Mode 
#+date:        [2024-11-10 So 20:13]
#+filetags:    :modes:eamcs:
#+identifier:  20241110T201337
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _88c37c32-6ddc-4ed7-9737-3a3c1b78a1d3:
.. INDEX:: line-number; 
.. INDEX:: ; line-number

.. meta::

   :description lang=de: Es gibt für dievers Aufgaben im Emacs spezielle Modes z.B. display-line-numbers-mode
   :keywords: EMACS, emacs, display-line-numbers-mode, Zeilennummern

.. raw:: html

   <style>

   .orange {
      color: orange;
   }
   
   </style>

.. role:: orange

.. image:: ./images/emacs--mode__display-line-numbers.avif
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/emacs--mode__display-line-numbers.avif')"></span>
   </a>
   
.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/emacs--mode__display-line-numbers.avif">
   </a>
   
.. sidebar:: Screenshot 

   |b|

   |a|




Für eine verbesserte Orientierung kann die Zeilennummerierung
aktiviert werden. Das hilft zum Beispiel bei der Programmierung, wenn
die Funktion »goto-line« nicht ausreicht. 

Wie üblich startet man mit »:kbd:`ESC-x`, :orange:`display-line-numbers-mode`  :kbd:`⏎`« 

#+END_export
