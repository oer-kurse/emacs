:PROPERTIES:
:ROAM_ALIASES: "Export -- zu  -- rst (Sphinx)"
:ID:           c0e31d34-0099-4da4-9c08-66809b5dfe91
:END:
#+title:       Export -- rst (Sphinx)
#+date:        [2024-11-20 Mi 19:38]
#+filetags:    :export:
#+identifier:  20241120T193822
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _c0e31d34-0099-4da4-9c08-66809b5dfe91:
.. INDEX:: Export; rst (Sphinx)
.. INDEX:: rst (Sphinx); Export
#+END_export

** Installation
Der Export in das Restructured Textformat (rst) liefert die Rohdaten
für die Transformation von Dokumente in diverse Ausgabeformate,
vorzugsweise zu HTML.

Dafür muss die Export-Bibliothek »ox-rst« istalliert werden:

- list-packages
- ox-rst , install

** Aktivieren des Export

#+BEGIN_SRC emacs-lisp 

  (require 'ox-rst)

#+END_SRC


[[./images/export-as-rst.png]]

** Weiterführende Anleitungen

Im Sphinx-Kurs werden die Details zu dem Dokumentations-Werkzeug
erläutert:

https://oer-kurse.gitlab.io/sphinx/

Für den schnellen »Workflow« nutze ich ein Python-Paket, das die
Org-Dateien temporär als rst-Datei speichert und anschließend mit
sphinx-autobuild daraus die finale HTML-Version. Die Detail sind über
den folgenden Link zu erreichen:

https://oer-kurse.gitlab.io/sphinx/extensions/org2sphinx/index.html


