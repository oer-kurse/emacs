:PROPERTIES:
:ROAM_ALIASES: "Export -- diverser Formate"
:ID:       8f6aaed2-0c58-4d1a-b4e4-0fbc35ea68d2
:END:
#+title:      Export -- diverser Formate
#+date:       [2024-05-01 Mi 13:43]
#+identifier: 20240501T134318
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil

#+BEGIN_export rst
.. _20240501T134318:
.. INDEX:: EMACS; Export 
.. INDEX:: Export; EMACS 
.. toctree::
   :maxdepth: 1
   :caption: 📁
   :glob:
   
   */index
   
   
.. toctree::
   :maxdepth: 1
   :caption: 📘
   :glob:
   
   *
#+END_export

#+begin_org
[[id:439650a5-b6d0-49ce-aa05-4116a4691a9a][Org-Mode]]
#+end_org
