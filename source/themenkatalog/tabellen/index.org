:PROPERTIES:
:ROAM_ALIASES: "Tabellen"
:ID:       f8e5ed34-54d6-4328-bba3-a8be6b6f391d
:END:
#+title:      Tabellen -- Berechnungen
#+date:       [2024-05-03 Fr 21:57]
#+identifier: 20240503T215729
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
.. [[id:7b5c5125-b886-44f6-9928-6a025cb34aa0][EMACS]]
#+end_org
#+BEGIN_export rst
.. _20240503T215729:

.. meta::

   :description lang=de: Emacs -- Berechnungen in Tabellen
   :keywords: Emacs, oer-kurse, Berechnugen, Tabellen, Formeln
   
.. INDEX:: Tabellen; Berechnung 
.. INDEX:: Berechnung; Tabellen

.. toctree::
   :maxdepth: 1
   :caption: 📘
   
   tastenkombinationen--fuer__tabellen
   tabelle--formeln__symbole
   tabellen-formeln-fuer__berechungen
   tabellen-formeln-laufende-nummer
   tabellen-formeln-berechnung-pro-zeile
   tabellen-formeln-summe
   tabellen-formeln-berechnung-komplett
   tabelle--neu-berechnen-aller__formeln
   tabelle--beispiel__fahrtenbuch
   
#+END_export
