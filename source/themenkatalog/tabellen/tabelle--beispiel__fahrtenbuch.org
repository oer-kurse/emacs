:PROPERTIES:
:ROAM_ALIASES: "Beispile -- Fahrtenbuch"
:ID:           a58e9161-d423-4da6-9de4-9b3fee814a8d
:END:
#+title:       Beispiel --  -- Fahrtenbuch
#+date:        [2025-01-04 Sa 11:50]
#+filetags:    :tabelle:
#+identifier:  20250104T115028
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _a58e9161-d423-4da6-9de4-9b3fee814a8d:
.. INDEX:: Beispiel; Fahrtenbuch
.. INDEX:: Fahrtenbuch; Beispiel

.. meta::

   :description lang=de: Beispiel Tabelle in EMACS mit Berechnungen Preis/Liter und gefahrene Kilometer
   :keywords: EMACS, Tabellen, Berechnungen
   
#+END_export

Ein Beispiel, für ein Fahrtenbuch, nach dem Eintrag von Datum, Menge
und Preis, wird der Preis pro Liter automatisch berechnet.


#+BEGIN_src bash
  |-+---------------+-------+-----+---------+------+----------+-----------+---------|
  | |Datum          | Menge |Preis|Preis pro|Extras|Kilometer-| Anmerkung |Kilometer|
  | |               |[Liter]|  [€]| Liter[€]|      |  stand   |           |pro Jahr |
  |-+---------------+-------+-----+---------+------+----------+-----------+---------|
  |#|<2024-11-28 Do>| 38.96 |65.80|    1.689|      |    156716|           |7384     |
  |#|<2024-12-20 Fr>|       |     |    0.000| 14.99|          |Waschanlage|         |
  |#|<2024-11-19 Di>| 38.12 |63.62|    1.669|      |    155987|           |         |
  |#|<2024-09-18 Mi>| 35.70 |58.16|    1.629|      |    155318|           |         |
  |#|<2024-08-10 Sa>| 37.47 |65.16|    1.739|      |    154692|           |         |
  |#|<2024-06-27 Do>| 38.95 |70.07|    1.799|      |    153963|           |         |
  |#|<2024-06-27 Do>| 23.01 |43.22|    1.878| 19.99|    152806|Waschanlage|         |
  |#|<2024-06-08 Sa>| 40.77 |73.75|    1.809|      |    152806|           |         |
  |#|<2024-05-29 Mi>| 26.46 |50.78|    1.919|      |          |           |         |
  |#|<2024-04-14 Do>| 41.18 |80.26|    1.949|      |    151554|           |         |
  |#|<2024-02-22 Do>| 35.08 |68.37|    1.949|      |          |           |         |
  |#|<2024-01-21 So>| 37.59 |66.50|    1.769|      |          |           |         |
  |#|<2024-01-03 Mi>| 13.66 |24.03|    1.759| 13.99|    149332|Waschanlage|         |
  |-+---------------+-------+-----+---------+------+----------+-----------+---------|
  #+TBLFM: $5=$4/$3;%.3f:: Preis pro Liter
  #+TBLFM: @3$9=@3$7-@>$7:: gefahrene Kilometer
    
#+end_src
