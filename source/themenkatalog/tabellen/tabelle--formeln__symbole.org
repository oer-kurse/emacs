:PROPERTIES:
:ROAM_ALIASES: "Tabelle Formeln -- Symbole"
:ID:       5517e4f4-f5ee-4fd1-aa39-a346c0cd75a8
:END:
#+title:      Tabelle -- Formeln Symbole
#+date:       [2024-05-07 Di 20:28]
#+identifier: 20240507T202834
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+BEGIN_export rst
.. _20240507T202834:
.. INDEX:: Tabelle; Sybole
.. INDEX:: Sybole; Tabelle
#+END_export
#+begin_org
[[id:f8e5ed34-54d6-4328-bba3-a8be6b6f391d][Tabellen]]
#+end_org

Spaltenzählung beginnt mit 1.

| Symbol        | Bedeutung             | Hinweis                        |
| \$            | eine Spalte           |                                |
| @             | eine Zeile            |                                |
| @3$4          | eine Zelle            | Beispiel Zeile 3 und Spalte 4  |
| @#            | laufende Nummer       | mit 0 beginnend                |
| @#-1          | laufende Nummer       | mit 1 beginnend                |
| @>$3          | Letzte Zeile          | in Spalte 3                    |
| vsum(@2..@-1) | Summe in einer Spalte | ab Zeile 2 bis vorletzte Zeile |

Eine Berechungszeile beginnt unterhalb der Tabelle mit folgender allgemeinen Zeile:

#+begin_src bash
  #+tblfm: FORMEL :: Kommentar
#+end_src
