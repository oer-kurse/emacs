:PROPERTIES:
:ROAM_ALIASES: "Tabellen -- Formeln -- Berechnung pro Zeile"
:ID:       0d903f27-d4a2-4791-be09-30f21630c985
:END:
#+title:      Berechnung pro Zeile
#+date:       [2024-05-03 Fr 21:51]
#+identifier: 20240503T215115
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
[[id:f8e5ed34-54d6-4328-bba3-a8be6b6f391d][Tabellen]]
#+end_org

#+BEGIN_export rst
.. _20240503T215115:
.. meta::

   :description lang=de: Emacs -- Berechnungen in Tabellen (pro Zeile)
   :keywords: Emacs, oer-kurse, Berechnugen, Tabellen, Formeln

.. INDEX:: Tabellen; Berechnungen pro Zeile
#+END_export

Eine Berechnung pro Zeile.

#+begin_src bash

|-----+--------------+-------+------+-----------|
| Nr. | Wo           | Start | Ende | Differnez |
|-----+--------------+-------+------+-----------|
|     | Büro         |   461 | 1058 |       597 |
|     | Schalfzimmer |   568 | 1341 |       773 |
|     | Bad oben     |   413 |  427 |        14 |
|     | Bad unten    |     0 |    0 |         0 |
|     | Wohnzimmer   |   495 | 1001 |       506 |
|     | Küche        |    46 |  129 |        83 |
|-----+--------------+-------+------+-----------|
|     |              |       |      |           |
|-----+--------------+-------+------+-----------|
#+tblfm: $5=$4-$3           :: Differenz

#+end_src

