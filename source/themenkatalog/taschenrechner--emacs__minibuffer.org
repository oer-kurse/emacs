:PROPERTIES:
:ROAM_ALIASES: "Taschenrechner -- im -- Minibuffer"
:ID:           f4ca659f-2242-4c2a-b760-26989ea05000
:END:
#+title:       Taschenrechner (Minibuffer)
#+date:        [2024-12-22 So 19:06]
#+filetags:    :calc:
#+identifier:  20241222T190642
#+OPTIONS:     num:nil author:nil toc:nil date:nil
#+BEGIN_export rst
.. _f4ca659f-2242-4c2a-b760-26989ea05000:
.. INDEX:: Taschenrechner; Minibuffer
.. INDEX:: Minibuffer; Taschenrechner
.. INDEX:: quick-calc; Taschenrechner

.. raw:: html

   <style>

   .orange {
      color: orange;
   }
   </style>
   
.. role:: orange

#+END_export

** Kleine Berechnung

Werte addieren, Mehrwertsteuer berechnen, ... wenn es mit Kopfrechnen
nicht mehr realisiert werden kann, muß ein Taschenrechner ran. Im
EMACS braucht es keine andere App (Anwendung)! Einfach starten mit:

#+begin_export rst

:kbd:`M-x` gefolgt von der Eingabe:  :orange:`quick-calc`

#+end_export

nun sind einfache Berechnungen möglich.

[[./images/emacs--taschenrechner__minipuffer.png]]


Quelle: https://codelearn.me/2024/12/22/emacs-quick-calc.html
